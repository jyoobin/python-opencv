#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 08:46:07 2018

@author: yoobin
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 13:42:07 2018

@author: yoobin
"""

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

img = cv.imread('frame.png',0)
img2 = img.copy()
template = cv.imread('hip.png', 0)
w, h = template.shape[::-1]

methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
            'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']

for meth in methods:
    img3 = img2.copy()
    method = eval(meth)
    res = cv.matchTemplate(img3, template ,method)
    min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
    
    if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
        
    cv.rectangle(img,top_left, bottom_right, 0, 7)
    
    plt.subplot(121),plt.imshow(res,cmap = 'gray')
    plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(img,cmap = 'gray')
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(meth)
    plt.show()



method = cv.TM_CCORR_NORMED
cap = cv.VideoCapture('RyanRun.mp4')
frame_count = 0
first_frame= True
hip = []
while (cap.isOpened()):
    frame_count = frame_count + 1
    if frame_count <= 870 :
        cap.grab()
    else:
        if frame_count == 1082:
            break
        cap.grab()
        ret, frame = cap.retrieve()
        grayscale = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        
        x = [l[0] for l in hip]
        y = [l[1] for l in hip]
        
        if (first_frame):
            res = cv.matchTemplate(grayscale, template, method)
            first_frame = False
        else:
            try:
                res = cv.matchTemplate(grayscale[y[-1]-5 : y[-1]+5, x[-1]-5: x[-1]+5],template, method)
            except:
                res = cv.matchTemplate(grayscale, template, method)

            
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        
        if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        
        cv.rectangle(grayscale, top_left, bottom_right, 255, 2)
        hip.append(top_left)
            
        cv.imshow('RyanRun', grayscale)
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
        

cap.release()
cv.destroyAllWindows()

x = [l[0] for l in hip]
y = [-1*l[1] for l in hip]
plt.plot(x,y)
plt.xlabel('X Pixel')
plt.ylabel('Y Pixel')


